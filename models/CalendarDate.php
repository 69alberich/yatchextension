<?php namespace Qchsoft\YatchExtension\Models;

use Qchsoft\YatchExtension\Models\Calendar;
use Qchsoft\YatchExtension\Models\DatePrice;
use Db;
use Model;

/**
 * Model
 */
class CalendarDate extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_calendardate';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = ["calendar_id", "date"];
    
    public $belongsTo = [
        'calendar' => Calendar::class
    ];

    public $hasMany = [
        'prices' => [DatePrice::class, 'key' => 'date_id']
    ];

    public function listExtendQuery($query, $id = null){
        $query->where("calendar_id", 1);
    }

    public function listCurrencies($fieldName, $value, $formData){
        $currencies = Db::table('lovata_shopaholic_currency as a')->where("active", 1)->lists('a.name', 'a.id');
        return $currencies;
    }

    public function listBookingTypes($fieldName, $value, $formData){
        
        $bookingTypes = Db::table('qchsoft_yatchextension_bookingtype as a')
        ->join('qchsoft_yatchextension_product_bookingtype as b', 'a.id', '=', 'b.booking_type_id')
        ->where('b.product_id', $this->calendar->calendarable_id)
        ->lists('a.name', 'a.id');
        return $bookingTypes;

    }

    public function listDestinations($fieldName, $value, $formData){
        $destination = Db::table('qchsoft_yatchextension_destination as a')
        ->join('qchsoft_yatchextension_product_destination as b', 'a.id', '=', 'b.destination_id')
        ->where('b.product_id', $this->calendar->calendarable_id)
        ->lists('a.name', 'a.id');
        return $destination;
    }
}
