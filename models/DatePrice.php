<?php namespace Qchsoft\YatchExtension\Models;

use Qchsoft\YatchExtension\Models\CalendarDate;
use Qchsoft\YatchExtension\Models\BookingType;
use Qchsoft\YatchExtension\Models\Destination;
use Lovata\Shopaholic\Models\Currency;
use Model;

/**
 * Model
 */
class DatePrice extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_dateprice';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = ["price", "date_id", "booking_type_id", "currency_id", "destination_id"];
    public $belongsTo = [
        'date' => CalendarDate::class,
        'booking_type' => BookingType::class,
        'destination' => Destination::class,
        'currency' => Currency::class,
    ];
}
