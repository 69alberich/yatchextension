<?php namespace Qchsoft\YatchExtension\Models;

use Model;

/**
 * Model
 */
class UpsellingCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_upselling_category';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
