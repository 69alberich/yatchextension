<?php namespace Qchsoft\YatchExtension\Models;

use Model;
use Qchsoft\YatchExtension\Models\BookingType;
use Qchsoft\YatchExtension\Models\Destination;
use Lovata\Shopaholic\Models\Product;

/**
 * Model
 */
class BookingRule extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_booking_rules';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'booking_type' => BookingType::class,
        'destination' => Destination::class,
        'product' => Product::class,
    ];
}
