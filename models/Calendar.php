<?php namespace Qchsoft\YatchExtension\Models;

use Model;
use Qchsoft\YatchExtension\Models\CalendarDate;
use Session;
/**
 * Model
 */
class Calendar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_calendar';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'dates' => CalendarDate::class
    ];

    public $morphTo = [
        'calendarable' => []
    ];

    public function getFirstDate(){
        $today = new \DateTime();
        return $this->dates()->where("quantity", ">", 0)
        ->where("date", ">=", $today->format("Y-m-d"))->first();
    }

    public function getLastDate(){
        $today = new \DateTime();
        return $this->dates()->where("quantity", ">", 0)
        ->where("date", ">=", $today->format("Y-m-d"))
        ->orderBy("date", "desc")->first();
    }

    public function getAllDates(){
        
        $today = new \DateTime();
        $dates = $this->dates()->where("quantity", ">", 0)
        ->where("date", ">=", $today->format("Y-m-d"))
        ->select("id", "date", "quantity")->get();
        
        return $dates->toArray();
        
    }
}
