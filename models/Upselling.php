<?php namespace Qchsoft\YatchExtension\Models;

use Model;
use  Qchsoft\YatchExtension\Models\UpsellingCategory;

/**
 * Model
 */
class Upselling extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_upsellings';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'category' => [UpsellingCategory::class, 'key' => 'category_id']
    ];
}
