<?php namespace Qchsoft\YatchExtension\Models;

use Model;

use Qchsoft\YatchExtension\Models\Calendar;
use Qchsoft\YatchExtension\Models\BookingType;
use Qchsoft\YatchExtension\Models\Destination;
use Lovata\Shopaholic\Models\Product;


use Lovata\Toolbox\Traits\Helpers\PriceHelperTrait;
/**
 * Model
 */
class Charge extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use PriceHelperTrait;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_charges';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable =[
    'destination_id', 'calendar_id', 'price',
    'first_date', 'last_date', 'booking_type_id', 'product_id', 'product_type'
    ];

    
    public $arPriceField = ['price', 'old_price', 'discount_price'];

    protected $fSavedPrice = null;
    protected $fSavedOldPrice = null;
    protected $arSavedPriceList = [];
    protected $iActivePriceType = null;
    protected $sActiveCurrency = null;


    public $belongsTo = [
        'booking_type' => BookingType::class,
        'destination' => Destination::class,
        'product' => Product::class,
        'calendar' => Calendar::class,
    ];


    public $morphMany = [
        'price_link' => [
            Price::class,
            'name'       => 'item',
            'conditions' => 'price_type_id is NOT NULL',
        ],
    ];
    public $morphOne = [
        'main_price' => [
            Price::class,
            'name'       => 'item',
            'conditions' => 'price_type_id is NULL',
        ],
    ];

    public function setActiveCurrency($sActiveCurrencyCode)
    {
        $this->sActiveCurrency = $sActiveCurrencyCode;

        return $this;
    }

    public function setActivePriceType($iPriceTypeID)
    {
        $this->iActivePriceType = $iPriceTypeID;

        return $this;
    }

}
