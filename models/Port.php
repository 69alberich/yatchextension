<?php namespace Qchsoft\YatchExtension\Models;

use Model;

/**
 * Model
 */
class Port extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_yatchextension_ports';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'preview_image' => 'System\Models\File',
    ];
    
    public $attachMany = ['images' => 'System\Models\File'];

    public $hasMany = [
        'products' => 'Lovata\Shopaholic\Models\Product'
    ];

    /* scopes */

    public function scopeGetByCode($query, $arrayCodes){
        return $query->whereIn("code", $arrayCodes);
    }

    public function scopeGetBySlug($query, $slug){
        return $query->where("slug", $slug);
    }


}
