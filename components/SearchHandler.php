<?php namespace QchSoft\YatchExtension\Components;

use Lovata\Toolbox\Classes\Component\SortingElementList;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\Shopaholic\Classes\Item\PromoBlockItem ;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Store\ProductListStore;
use QchSoft\YatchExtension\Classes\Processor\SearchProductProcessor;
use QchSoft\YatchExtension\Models\Port;
use Redirect;
use Input;
use Carbon\Carbon;

class SearchHandler extends SortingElementList
{
    
    protected $mainPage;
    private $categories;
    private $ports;
    private $destinations;
    private $searchParams;

    /**
     * @return array
     */
    public function componentDetails(){
        return [
            'name'        => 'Search Handler',
            'description' => 'Methods using to find products',
        ];
    }

    public function defineProperties(){
        return [
            'mode' => [
                'title' => 'Tipo de busqueda',
                'type' => 'dropdown',
                'default' => 'query'
            ],
            'search_param' => [
                'title' => 'Variable de busqueda',
                'type' => 'string',
                'placeholder' =>'opcional'
            ],
            'main_page'=>[
                'title' => 'Pagina principal a donde redirigir',
                'type' => 'string',
                
            ]
    
        ];
    }
    public function onRun(){
  
        $this->categories = $this->initCategories();
        $this->ports = $this->initPorts();
        $this->destinations = $this->initDestinations();
        $this->page["searchParams"] = $this->searchParams = get();
        
        if(count($this->searchParams) > 0){
           
            $baseProductList = null;
 
            if($this->searchParams["searchType"] == "compra"){
                $this->page['obProductList'] = $this->quickSearch();
            }elseif($this->searchParams["searchType"]=="renta"){
                $this->page['obProductList'] = $this->rentSearch();
            }
        }
        $this->mainPage = $this->property('main_page');
    }

    public function quickSearch(){
        
        $baseProductList = ProductCollection::make()->active();
        if($this->searchParams["category"]!="all"){
            
            $category = CategoryCollection::make()->getByCode([$this->searchParams["category"]])->first();
            //trace_log($category);
            if($category)
                $baseProductList->category($category->id);
        }
        
        if($this->searchParams["port"]!="all"){
            $port = Port::getByCode([$this->searchParams["port"]])->first();
            if($port)
                $baseProductList->getByPort($port->id);
        }
        
       
        return $baseProductList;
    }

    public function rentSearch(){

        $baseProductList = null;
        $arrayIds = SearchProductProcessor::searchByDates($this->searchParams, 10);
        if(count($arrayIds)> 0){
            $baseProductList = ProductCollection::make($arrayIds)->active();
        }
        return $baseProductList;

    }

    public function getInitialDates(){
        $dates = [];

        if (isset($this->searchParams["dates"])) {
            $dates  = explode(" - ", $this->searchParams["dates"]);
        }else{
            $today = new Carbon();
            $endDate = new Carbon();
            $endDate->addDays(7);
            $dates[0] = $today->format("d-m-Y");
            $dates[1] = $endDate->format("d-m-Y");
        }

        return $dates;
    }
    public function onSearch(){
        $data = post();
        if($this->property('main_page') == ""){
            $this->page["searchParams"] = $this->searchParams = $data;
            if(count($this->searchParams) > 0){
            
                $baseProductList = null;
                if($this->searchParams["searchType"] == "compra"){
                    $this->page['obProductList'] = $this->quickSearch();
                }elseif($this->searchParams["searchType"]=="renta"){
                    $this->page['obProductList'] = $this->rentSearch();
                }
            }
        }else{
            return Redirect::to('busqueda/q?'.http_build_query($data));
        }
    }
    
    public function onFilter(){
        $this->searchParams = post();
        $baseProductList = null;
        if(!isset($this->searchParams["searchType"]) || $this->searchParams["searchType"]=="compra"){
            $baseProductList = $this->filterProductsForBuy();
        }elseif($this->searchParams["searchType"]=="renta"){
            $baseProductList = $this->filterProductsForRent();
        }
        $this->page["searchParams"] = $this->searchParams;
        $this->page['obProductList'] = $baseProductList;
        
        if (isset($this->searchParams["page"])) {
            $this->page['page'] = $this->searchParams["page"];
        }else{
            $this->page['page'] = 1;
        }
        
    }

    public function filterProductsForBuy(){
        $baseProductList = null;
        $brands = $this->getBrandsArray();
        $categories = $this->getSubCategoriesArray();
        $arrayIds = array();
       
        if(count($categories)>0){
            $baseProductList = ProductCollection::make()->category($categories)->active()->sort("price|asc");
        }
        elseif($this->searchParams["base"] == "category"){
            $baseProductList = ProductCollection::make()->category($this->searchParams["basev"], true)->active()->sort("price|asc");
        }elseif($this->searchParams["base"] == "promo"){
            $obPromoBlockItem = PromoBlockItem::make($this->searchParams["basev"]);
            if ($obPromoBlockItem->product) {
                $baseProductList = $obPromoBlockItem->product;
            }else{
                $baseProductList = null;
            }
        }elseif($this->searchParams["base"] == "brand"){
            $baseProductList = ProductCollection::make()->brand($this->searchParams["basev"])->active();

        }elseif($this->searchParams["base"] == "port"){
            $baseProductList = ProductCollection::make()->getByPort($this->searchParams["basev"])->active();
        }elseif($this->searchParams["base"] == "search" || $this->searchParams["base"] == "busqueda"){  
            $baseProductList = $this->quickSearch();
        }

        if (count($brands)>0) {
            $baseProducts = Product::brands($brands)->select("id")->get();
            $arrayIds = $this->getProductsId($baseProducts);
            $baseProductList = ProductCollection::make($arrayIds)->active();
            if($this->searchParams["base"]=="category"){
                $baseProductList->category($this->searchParams["basev"],true);
            }
            
        }
        //ORDEN
        if(isset($baseProductList) && isset($this->searchParams["sort"])){
         
            switch ($this->searchParams["sort"]) {
                case '1':
                    $baseProductList->sort("new");
                    break;
                case '2':
                    $baseProductList->sort("price|asc");
                    break;
                case '3':
                    $baseProductList->sort("price|desc");
                    
                    break;
                default:
                $baseProductList->sort("no");
                    break;
            }
        }
        return $baseProductList;
    }

    public function filterProductsForRent(){
        $baseProductList = $this->rentSearch();
        $brands = $this->getBrandsArray();
        $categories = $this->getSubCategoriesArray();

        if (count($brands)>0) {
            
            $baseProductList->getByBrandIds($brands);
            if($this->searchParams["base"]=="category"){
                $baseProductList->category($this->searchParams["basev"],true);
            }
            
        }
        //ORDEN
        if(isset($baseProductList) && isset($this->searchParams["sort"])){
            
            switch ($this->searchParams["sort"]) {
                case '1':
                    $baseProductList->sort("new");
                    break;
                case '2':
                    $baseProductList->sort("price|asc");
                    break;
                case '3':
                    $baseProductList->sort("price|desc");
                    
                    break;
                default:
                $baseProductList->sort("no");
                    break;
            }
        }
        return $baseProductList;
    }

    public function initCategories(){
        $categories = \Lovata\Shopaholic\Models\Category::whereNull("parent_id")->get();
        return $categories;
    }

    public function initPorts(){
        $ports = \QchSoft\YatchExtension\Models\Port::all();
        return $ports;
    }

    public function initDestinations(){
        $destinations = \QchSoft\YatchExtension\Models\Destination::all();
        return $destinations;
    }

    public function getInitialCategories(){
        return $this->categories;
    }

    public function getInitialPorts(){
        return $this->ports;
    }

    public function getInitialDestinations(){
        return $this->destinations;
    }

    public function getSearchParam(){
        return $this->searchParams;    
    }
    /**
     * Make element collection
     * @param array $arElementIDList
     *
     * @return ProductCollection
     */
    public function make($arElementIDList = null){
        return ProductCollection::make($arElementIDList)->sort("no");;
    }

    public function getBrandsArray(){
        $brands = array();
        foreach ($this->searchParams as $key => $value) {

            if ($this->startsWith($key,"brand")) {
                array_push($brands, $value);
            }
        }
        return $brands;
    }

    public function getSubCategoriesArray(){
        $categories = array();
        foreach ($this->searchParams as $key => $value) {

            if ($this->startsWith($key,"subcategory")) {
                array_push($categories, $value);
            }
        }
        return $categories;
    }

    function startsWith ($string, $startString){ 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    function getProductsId($brandProduct){
        $array = array();

        foreach ($brandProduct as $key => $value) {
       
            if (!in_array($value->id, $array)) {
                array_push($array, $value->id);
            }
        }
        return $array;
    }

    public function getModeOptions(){
        return [
            'query'=>'Busqueda simple',
            'category'=>'Categoría',
            'brand' =>'Marca'
            ];
    }
}