<?php namespace QchSoft\YatchExtension\Components;

use BackendAuth;
use Cms\Classes\ComponentBase;

class BackendUserHandler extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name' => 'BackendUserHandler',
            'description' => 'Methods for backend user session'
        ];
    }

    public function defineProperties(){
        return [];
    }

    public function getBackendUser(){
        $userMaster = BackendAuth::getUser();

        return $userMaster;
    }
}