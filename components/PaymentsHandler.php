<?php namespace QchSoft\YatchExtension\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use QchSoft\YatchExtension\Models\Payment;

use Input;
use Response;
use Mail;
use System\Models\File;
use Lovata\Shopaholic\Models\Settings as ShopaHolicSettings;
//use QchSoft\ShopPlus\Models\Settings as ShopPlusSettings;

class PaymentsHandler extends ComponentBase{
    
    protected $obUser;
    protected $obOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Payments Handler',
            'description' => 'test',
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title' => 'Code',
                'description' => 'Code used to find order',
                'type' => 'string',
                'required' => 'true',

            ],
            'type' => [
                "title" => 'Type',
                'description' => 'find by code or slug',
                'type' => "dropdown",
                'options'     => ['number'=>'by number', 'slug'=>'by slug']
            ]
        ];
    }
    public function init(){
        
        

        $user = UserHelper::instance()->getUser();
        
        //var_dump($this->obOrder);
       // trace_log("tengo:".$this->property('code'));
        if ($this->property('type') == "slug") {
            $this->obOrder =  Order::getBySecretKey($this->property('code'))->first();
        }else{
            $this->obOrder =  Order::getByNumber($this->property('code'))->first();
        }
        
        if ($user == null) {
            $user = UserHelper::instance()->findUserByEmail($this->obOrder->user->email);
        }

        $this->obUser = $user;
    }

    public function onAddPayment(){
        $data = post();
        trace_log($data);
        $file = Input::file("capture");
       // trace_log(get_class($this->obUser));
        $nospace = str_replace(" ", "", $this->obOrder->total_price);
        $mount = str_replace(",", ".", $nospace);

        $payment = new Payment();

        $payment->order_id = $this->obOrder->id;
        $payment->mount = $mount;
        $payment->reference = $data["reference"];
        $payment->payment_method_id = $data["method_id"];
        $payment->currency = 1;
        $payment->payment_status_id = $data["status_id"];
        $payment->usable_id = $this->obUser->id;
        $payment->usable_type = get_class($this->obUser);
        $payment->file = $file;
        /*
        if($file != null ){
            $fileToUpload = new File;
            $fileToUpload->fromPost($file);
            
            $fileToUpload->save();

            $payment->file()->add($fileToUpload);
            //$payment->file = $file;
            $payment->save();
        }*/
        $this->obOrder->status_id = $data["order_status_id"];
        $this->obOrder->payment_method_id = $data["method_id"];
        if (isset($data["subsidiary"]) && $data["subsidiary"]!=null) {
            $this->obOrder->property["subsidiary"]= $data["subsidiary"];
        }
        
        if($payment->save() && $this->obOrder->save()){
            
            $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
            $arOrder = array("order" => $this->obOrder->toArray());
            Mail::send("qchsoft.yatchextension::mail.payment_received",
                $arOrder, function($message) use ($arOrder, $payment, $emailList) {
                foreach ($emailList as $email) {
                    $message->to($email);
                }
                //$message->to("alberich2052@gmail.com");
                if($payment->file){
                    $message->attach($payment->file->getPath());
                }
            });

            return Response::json(
            ['success' => true, 
             'payment_id' => $payment->id,
             'mount'=> $this->getConvertedMount($payment->mount),
             'currency' => $this->obOrder->currency->code,
             //'shipping_type' => $this->obOrder->shipping_type->name,
             //'payment_method' => $this->obOrder->payment_method["name"],
             ]);

        }else{
            return Response::json(['success' => false]);
        }
        
    }

    /*public function onPayInSite(){
        $data = post();
       
        $this->obOrder->status_id = 6;
        $this->obOrder->payment_method_id = 5;

        $orderP = $this->obOrder->property;
        $orderP["subsidiary"] = $data["subsidiary"];

        $this->obOrder->addJsonable("property", $orderP);

        if($this->obOrder->save()){
            return Response::json(['success' => true]);
        }else{
            return Response::json(['success' => false]);
        }
    }*/

    public function getConvertedMount($mount){
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
        $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();
      
       if($activeCurrencyCode == "USD"){
        $convertedPrice = $mount;
       }else{
        $convertedPrice = $mount/$defaultCurrency->rate;
       }
       return $convertedPrice;
    }
}