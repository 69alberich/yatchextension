<?php namespace QchSoft\YatchExtension\Components;

use QchSoft\YatchExtension\Classes\Processor\SearchProductProcessor;
use QchSoft\YatchExtension\Classes\Processor\CustomOrderProcessor;
use Lovata\OrdersShopaholic\Components\MakeOrder;
use Lovata\Toolbox\Classes\Helper\UserHelper;

use Input;
use Session;
use Validator;
use Flash;
use Response;
use BackendAuth;
class CartSessionList extends MakeOrder
{
    
    /** @var \Lovata\Buddies\Models\User */
    protected $obUser;

    public function componentDetails()
    {
        return [
            'name'        => 'Cart Session List',
            'description' => 'List of Charges in Session',
        ];
    }


    public function onAddCharge(){
      
        $data = post();

        $session = Session::all();
        

        $rule = $this->getMinDays($data);
        $arrDates = explode(" - ", $data["dates"]);

        //trace_log("minimo:".$rule->min_days);

        if ($this->existIncart($data) == true) {
          Flash::warning("Ya has seleccionado la misma fecha anteriormente, elige una fecha diferente");
        }elseif(isset($rule->min_days) && count($arrDates)==2){
         
          $begin = new \DateTime($arrDates[0]);
          $end = new \DateTime($arrDates[1]);
          $diff = $end->diff($begin)->format("%a");
          if ($diff < $rule->min_days) {
            Flash::warning("Debes elegir al menos ".$rule->min_days." días para poder alquilar este paseo");
          }else{
            $result = $this->getCartPositionInfo($data);
            $this->applySelection($result, $data);
          }
          
        }else{
          $result = $this->getCartPositionInfo($data);
          $this->applySelection($result, $data);
        }
    

        /*if (isset($session["charges"]) && $session["charges"]!=null) {
          $array = $session["charges"];
          array_push($array, $data);
            Session::put('charges', $array);
          
        }else{
          $array = array("0" => $data);
          Session::put("charges", $array);
        } */  

    
      /*$validator = Validator::make(
          [
          'price' => $data["price"],
          'title' => $data["title"],
          ],
          [
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'title' => 'required',
          ],
          ['price.regex' => "Formato de precio incorrecto",
          'title.required' => 'Titulo es requerido',
          'price.required' => 'Monto es requerido',
          ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $retorno = "";
            foreach ($messages->all('<li>:message</li>') as $message) {
              $retorno .= $message;
            }
            return  Flash::error($retorno);
        }else{

         if (isset($session["charges"]) && $session["charges"]!=null) {
            $array = $session["charges"];
            array_push($array, $data);
              Session::put('charges', $array);
            
          }else{
            $array = array("0" => $data);
            Session::put("charges", $array);
          }   
        }
        */
      }

    public function existIncart($data){
      $session = Session::all();

      $flag = false;
      if (isset($session["charges"]) && $session["charges"]!=null) {
        $current_array = $session["charges"];

        foreach ($current_array as $cartItem) {
      
          if($cartItem["product_id"] == $data["id"] && $cartItem["dates"] == $data["dates"]){
            $flag = true;
          }
        }

      }
      return $flag;
    }

    public function getMinDays($data){
      
      $result = SearchProductProcessor::getMinDaysForBooking($data);

    
      return $result;
    }
    public function onRemoveCharge(){
      $data = Input::get();
      $session = Session::all();

      if (isset($session["charges"]) && $session["charges"]!=null) {
        $current_array = $session["charges"];
      
        unset($current_array[$data["index"]]);
        $new_array = array();
        foreach ($current_array as $key => $value) {
          array_push($new_array,$value);
        }
        Session::put('charges', $new_array);
      }
    }

    public function getSessionCartList(){
      
      $session = Session::all();
      if(isset($session["charges"])){
        return $session["charges"];
      }else{
        return null;
      }
      
    }

    public function moneyFormat($mount){
        $value = number_format($mount, 2 ,",", "." );
        return $value;
    }

    public function onCreateOrder(){
      $obOrder = null;
      $session = Session::all();
      $user = UserHelper::instance()->getUser();
      $userMaster = BackendAuth::getUser();
      
      $data = Input::get();
      $charges = null;

      if(isset($session["charges"])){
        $charges = $session["charges"];
      }elseif(isset($data["charges"])){
        $charges = $data["charges"];
      }
  
      $validatorMessage = "";
      if ($charges) {
        if(count($charges) <1 ){
          $validatorMessage.="*Debes crear al menos 1 cargo<br>";
        }
      }else{
        $validatorMessage="*Tu carrito está vacío, añade al menos un bote<br>";
      }
      
      if(!$userMaster && !$user){
        $validatorMessage.="*Tu sesión ha caducado, refresca esta página e inicia sesión<br>";
      }
      
      if($data["name"] == ""){
        $validatorMessage.="*Debes llenar el campo nombre<br>";
      }

      if($data["last_name"] == ""){
        $validatorMessage.="*Debes llenar el campo apellido<br>";
      }

      if($data["document"] == "" || strlen($data["document"]<6)){
        $validatorMessage.="*Debes llenar el campo documento con al menos 6 dígitos<br>";
      }

      if($data["phone"] == "" || strlen($data["phone"]<7)){
        $validatorMessage.="*Debes llenar el campo teléfono con al menos 7 dígitos<br>";
      }
      if ($validatorMessage != "") {
         Flash::error($validatorMessage);
      }else{
        $obOrder = CustomOrderProcessor::instance()->createOrder($charges, $data, $user);
        //trace_log($data);
        //trace_log($charges);
      }
      //trace_log($validatorMessage);
      //return false;
      if($obOrder != null){
        
        CustomOrderProcessor::instance()->updateCalendar($obOrder, "reduce");
        Session::forget("charges");
        $response =  Response::json(['success' => true, 
         'order_id' => $obOrder->id,
         'order_number' => $obOrder->order_number,
         'secret_key' =>$obOrder->secret_key,
         ]);
      }else{
        //data["error"][0]
          $response =  Response::json(['success' => false, 'error'=>[$validatorMessage]]);
      }
      return $response;
    }

    public function getCartPositionInfo($data){
      $result = SearchProductProcessor::getBoatPrice($data, false);
      return $result;
    
    }

    public function applySelection($result, $data){
      $charge = array();
      $session = Session::all();

      foreach ($result as $itemCharge) {
        if (isset($charge["price"])) {
          $charge["price"] += $itemCharge->price;
        }else{
          $charge["price"] = $itemCharge->price;
        }
        $charge["name"] = $itemCharge->productName;
        $charge["product_id"] = $itemCharge->productId;
        $charge["destination"] = $itemCharge->destination;
        $charge["bookingType"] = $itemCharge->bookingType;
        $charge["dates"] = $data["dates"];
        $charge["booking_type_id"] = $itemCharge->booking_type_id;
        $charge["destination_id"] = $itemCharge->destination_id;
        $charge["calendar_id"] = $itemCharge->calendar_id;
      }

      if (isset($session["charges"]) && $session["charges"]!=null) {
        $array = $session["charges"];
        array_push($array, $charge);
          Session::put('charges', $array);
        
      }else{
        $array = array("0" => $charge);
        Session::put("charges", $array);
      } 

    }
}
