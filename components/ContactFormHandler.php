<?php namespace QchSoft\YatchExtension\Components;

use Cms\Classes\ComponentBase;
use Input;
use Response;
use Mail;
use Lovata\Shopaholic\Models\Settings as ShopaHolicSettings;
//use QchSoft\ShopPlus\Models\Settings as ShopPlusSettings;

class ContactFormHandler extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name'        => 'Contact Form Handler',
            'description' => 'methods for contact form',
        ];
    }


    public function onSentForm(){
        $data = post();
        trace_log($data);
        
        $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
        $form = array("form" => $data);
        
        try {
            Mail::send("qchsoft.yatchextension::mail.contact-form",
            $form, function($message) use ($emailList) {
            foreach ($emailList as $email) {
                $message->to($email);
            }
            //$message->to("alberich2052@gmail.com");
            
            });
            return Response::json(['success' => true]);
            
        } catch (\Throwable $th) {

            return Response::json(['success' => false]);

        }
        

        

    
        //
        
        
    }
    
}