<?php namespace QchSoft\YatchExtension\Components;

use Cms\Classes\ComponentBase;

use QchSoft\YatchExtension\Classes\Processor\SearchProductProcessor;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;

use Db;
use Session;
use Flash;
class BoatBookingHandler extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name' => 'BoatBookingHandler',
            'description' => 'Methods for booking boats'
        ];
    }

    public function defineProperties(){
        return [];
    }

    public function onFindBoatPrice(){
        $post = post();
        $messages = "";

        $post = post();
        //trace_log($post);
        $result = SearchProductProcessor::getBookingProductMode($post);
    
        if ($post["bookingType"] < 1) {
          $messages.="*Elige un tipo de paseo \n";
        }
        if (isset($result->is_multidate) &&  $result->is_multidate == 1) { //si es pernocta
        
          if (!$this->validateDateRange($post["dates"])) {
            $messages.="*Elige un rango de fechas correcto\n";
          }
          
        }
        if ($messages !="") {
          Flash::warning($messages);
        }else{
          $result = SearchProductProcessor::getBoatPrice($post, false);
          $this->applyResultSet($result, $post);
        }
        return true;
    }

    public function getPhoneCodes(){
      $service_url = 'https://restcountries.eu/rest/v2/all';
      $curl = curl_init($service_url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $curl_response = curl_exec($curl);
      if ($curl_response === false) {
          $info = curl_getinfo($curl);
          curl_close($curl);
          //die('error occured during curl exec. Additioanl info: ' . var_export($info));
          return array('+58' => '+58' );
      }
      curl_close($curl);
      $decoded = json_decode($curl_response);
      if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
          //die('error occured: ' . $decoded->response->errormessage);
         return array('+58' => '+58' );

      }else{
        return $decoded;
      }
    }

    public function onChangeBookingType(){
        $post = post();
        //trace_log($post);
        $result = SearchProductProcessor::getBookingProductMode($post);
      
        $this->page["bookingMultidate"] = $result->is_multidate;
        $this->page["price"] = null;
    }

    public function applyResultSet($result, $data){
        $total = 0;
        $confirm_dates = array();
        $flag = true;
        //trace_log($result);
        foreach ($result as $dateCalendar) {
          array_push($confirm_dates, $dateCalendar->date);
        }

        foreach ($result as $dateCalendar) {
          
          $total += $dateCalendar->price;

          if($dateCalendar->is_multidate == 1){
           
            $arrDates = explode(" - ", $data["dates"]);
            $begin = new \DateTime($arrDates[0]);
            $end = new \DateTime($arrDates[1]);
            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval, $end);
            
            foreach($daterange as $date){ 
              if (in_array($date->format("Y-m-d"), $confirm_dates) && $flag) {
              $flag = true;
            }else{
              trace_log($date);
              $flag = false;
            }
            }
            
          }

          if (!$flag) {
            $total = 0;
          }
        }
        

        
        $this->page["price"] = $total;
    }

    public function validateDateRange($dates){
      $arrDates = explode(" - ", $dates);
  
      if(count($arrDates) < 2){
        return false;
      }else{
        $begin = new \DateTime($arrDates[0]);
        $end = new \DateTime($arrDates[1]);
        if ($begin >= $end ) {
          return false;
        }
        return true;
      }

    }
}
/*
 foreach ($dates as $fecha) {
              array_push($fechas_confirmadas, $fecha->fecha);
            }
            $flag = true;
            foreach($daterange as $date){
                if (in_array($date->format("Y-m-d"), $fechas_confirmadas) && $flag) {
                $flag = true;
              }else{
                $flag = false;
              }
            }
            if ($flag) {
              $precio = $this->cargarPrecios($propiedades, $dates);
            }
 */