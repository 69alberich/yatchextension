<?php namespace Qchsoft\YatchExtension;

use System\Classes\PluginBase;

use Event;
use Crypt;
use QchSoft\YatchExtension\Classes\Processor\CustomOrderProcessor;

use QchSoft\YatchExtension\Classes\Event\Product\ExtendProductFieldsHandler;
use QchSoft\YatchExtension\Classes\Event\Product\ExtendProductModelHandler;
use QchSoft\YatchExtension\Classes\Event\Product\ExtendProductCollectionHandler;
use QchSoft\YatchExtension\Classes\Event\Product\ExtendProductItemHandler;

use QchSoft\YatchExtension\Classes\Event\Order\ExtendOrderControllerHandler;
use QchSoft\YatchExtension\Classes\Event\Order\ExtendOrderModelHandler;

use QchSoft\YatchExtension\Classes\Event\OrderPosition\ExtendOrderPositionModelHandler;

use QchSoft\YatchExtension\Classes\Event\Settings\ExtendSettingsFieldHandler;

use QchSoft\YatchExtension\Classes\Event\Category\ExtendCategoryCollectionHandler;

use Lovata\Shopaholic\Classes\Collection\BrandCollection;

class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'QchSoft\YatchExtension\Components\SearchHandler'   => 'SearchHandler',
            'QchSoft\YatchExtension\Components\BoatBookingHandler'   => 'BoatBookingHandler',
            'QchSoft\YatchExtension\Components\CartSessionList'   => 'CartSessionList',
            'QchSoft\YatchExtension\Components\BackendUserHandler'   => 'BackendUserHandler',
            'QchSoft\YatchExtension\Components\PaymentsHandler'   => 'PaymentsHandler',
            'QchSoft\YatchExtension\Components\ContactFormHandler'   => 'ContactFormHandler',
            
        ];
    }

    public function registerSettings(){

    }

    public function registerMarkupTags(){
        return [
            'functions' => [
                'getFirstAvailableOffer' => function ($obProdct) {return $this->getFirstAvailableOffer($obProdct);},
                'getBrandsFromResult' => function($obProductList){return $this->getBrandsFromResult($obProductList);},
                'encrypt' => function($value){return $this->encrypt($value);}
            ],
        ];
    }
    
    private  function getFirstAvailableOffer($obProdct){
        //trace_log($obProdct);
        $offers = $obProdct->offer;
        $firstOffer = null;
        foreach ($offers as $offer) {
            if($offer->price_value > 0 && $offer->quantity > 0 && $firstOffer == null){
                $firstOffer = $offer;
            }
        }
        return $firstOffer;
    }

    private function getBrandsFromResult($obProductList){
        $brandsIds = array();
        foreach ($obProductList as  $product) {
            $brandId = $product->brand->id;
            if ($brandId != null && !in_array($brandId, $brandsIds)) {
                array_push($brandsIds, $product->brand->id);
            }
        }
        return BrandCollection::make($brandsIds);
    }

    private function encrypt($value){
        $secret = Crypt::encrypt($value);
        return $secret;
    }

    public function registerSchedule($schedule)
    {
        //trace_log("pase");
        $schedule->call(function () {
            
           //trace_log("llegue");
           $arOrders = OrderCollection::make()->status(1);
            
            //trace_log("por procesar:".count($arOrders));
            
            foreach ($arOrders as $obOrderItem) {
                $obOrder = $obOrderItem->getObject();
                if($obOrder->isAvailableToPay()){

                    $emailList = explode(",", Settings::get("creating_order_manager_email_list"));
                    
                    $arOrder = [
                        'order'        => $obOrder,
                        'order_number' => $obOrder->order_number,
                        'site_url'     => config('app.url'),
                    ];
                    Mail::send("lovata.ordersshopaholic::mail.create_order_user",
                        $arOrder, function($message) use ($arOrder, $obOrder) {
                            $message->to($obOrder->user->email);
                        
                    });
                    //trace_log("aqui");
                }else{
                    $obOrder->status_id = 6;
                    CustomOrderProcessor::instance()->updateCalendar($obOrder, "restore");
                    $obOrder->save();

                    //trace_log("alla");
                }
                
           }
           // \Db::table('recent_users')->delete();
        })->hourly();
    }

    public function boot(){
        Event::subscribe(ExtendProductFieldsHandler::class);
        Event::subscribe(ExtendProductModelHandler::class);
        Event::subscribe(ExtendProductCollectionHandler::class);
        Event::subscribe(ExtendProductItemHandler::class);
        Event::subscribe(ExtendCategoryCollectionHandler::class);
        Event::subscribe(ExtendOrderControllerHandler::class);
        Event::subscribe(ExtendOrderModelHandler::class);
        Event::subscribe(ExtendOrderPositionModelHandler::class);
        Event::subscribe(ExtendSettingsFieldHandler::class);
    }
}
