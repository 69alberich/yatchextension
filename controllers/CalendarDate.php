<?php namespace Qchsoft\YatchExtension\Controllers;

use QchSoft\YatchExtension\Classes\Processor\DatePricesProcessor;
use Qchsoft\YatchExtension\Models\Calendar;
use Qchsoft\YatchExtension\Models\CalendarDate as CalendarDateModel;
use Backend\Classes\Controller;
use BackendMenu;
use Lang;
use Flash;

class CalendarDate extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    
    protected $calendarId;
    protected $obGenerateFormWidget;
    protected $obCalendar;
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qchsoft.YatchExtension', 'port-item', 'side-menu-item6');
    }

    public function index($calendarId = null){

        if($calendarId){
            $this->calendarId = $calendarId;
            $this->obCalendar = Calendar::find($calendarId);
        }
        
        $this->asExtension('ListController')->index();
    }

    public function listExtendQuery($query){
        // Extend the list query to filter by the user id
        
        if ($this->calendarId)
            $query->where('calendar_id', $this->calendarId);
               
    }

    public function generate($iRecordID, $sContext = null){
        if($iRecordID){
            $this->calendarId = $iRecordID;
        }

        //$obModel = $this->formFindModelObject($iRecordID);
        $obModel = new CalendarDateModel();

        $this->obCalendar = Calendar::find($this->calendarId);

        $obModel->calendar()->add($this->obCalendar);
        

        $config = $this->makeConfig('$/qchsoft/yatchextension/models/calendardate/generate.yaml');

        $this->pageTitle = Lang::get('Generate dates');

        $config->model = $obModel;
        $config->arrayName = class_basename($obModel);
        $config->context = $sContext;

        $this->obGenerateFormWidget = $this->makeWidget('Backend\Widgets\Form', $config);
    }

    public function availability($iRecordID, $sContext = null){
        if($iRecordID){
            $this->calendarId = $iRecordID;
        }

        $obModel = $this->formFindModelObject($iRecordID);

        $config = $this->makeConfig('$/qchsoft/yatchextension/models/calendardate/availability.yaml');

        $this->pageTitle = Lang::get('Change Avaiability');

        $config->model = $obModel;
        $config->arrayName = class_basename($obModel);
        $config->context = $sContext;

        $this->obGenerateFormWidget = $this->makeWidget('Backend\Widgets\Form', $config);
    }
    

    public function formGenerateRender($arOptionList = []){
        if (!$this->obGenerateFormWidget) {
            throw new ApplicationException(Lang::get('backend::lang.form.behavior_not_ready'));
        }

        return $this->obGenerateFormWidget->render($arOptionList);
    }

    public function onGenerate(){
        $form = post();
        $dateProcessor = new DatePricesProcessor();
        $form["CalendarDate"]["calendar_id"] = $form["calendar_id"];
        $dateProcessor->generateDates($form["CalendarDate"]);
        //trace_log(post());
        Flash::success("Proceso Completado");
        return true;
    }

    public function onChangeAvailability(){
        $form = post();
        $dateProcessor = new DatePricesProcessor();
        $form["CalendarDate"]["calendar_id"] = $form["calendar_id"];
        $dateProcessor->setAvailability($form["CalendarDate"]);
        //trace_log(post());
        Flash::success("Proceso Completado");
        return true;
    }
}
