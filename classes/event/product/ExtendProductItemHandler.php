<?php namespace Qchsoft\yatchextension\Classes\Event\Product;

use Lovata\Shopaholic\Classes\Item\ProductItem;

class ExtendProductItemHandler{

    public function subscribe(){
        
        ProductItem::extend(function($obProductItem) {
            /** @var ProductItem $obProductItem */
           
            $obProductItem->arExtendResult[] = 'destinations';
            $obProductItem->arExtendResult[] = 'port';
            $obProductItem->arExtendResult[] = 'minPrice';

            $obProductItem->addDynamicMethod('destinations', function() use ($obProductItem) {
                
                $obProduct = $obProductItem->getObject();
                $obProductItem->setAttribute('destinations', $obProduct->destinations->toArray());
                
                
             });

             $obProductItem->addDynamicMethod('port', function() use ($obProductItem) {
                $obProduct = $obProductItem->getObject();
                $obProductItem->setAttribute('port', $obProduct->port->toArray());
            });

            $obProductItem->addDynamicMethod('minPrice', function($searchParams = null) use ($obProductItem) {
                if($searchParams != null){
                    $obProduct = $obProductItem->getObject();
                    $obProductItem->setAttribute('minPrice',$obProduct->getMinPrice($searchParams));
                    return $obProduct->getMinPrice($searchParams);
                }else{
                    return 0;
                }
                
            });
        });
    }
}

