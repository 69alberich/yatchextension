<?php namespace Qchsoft\YatchExtension\Classes\Event\Product;

use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;

class ExtendProductCollectionHandler {

    public function subscribe(){
        ProductCollection::extend(function ($obProductList) {
            $this->addCustomMethod($obProductList);
        });
    }

    protected function addCustomMethod($obProductList)
    {
        
        $obProductList->addDynamicMethod('getByExternalId', function ($arrayCodes = null) use ($obProductList) {
            $idsImploded = implode(',',$arrayCodes);
            
            if($arrayCodes !=null ){
                $arResultIDList = (array) Product::whereIn("external_id", $arrayCodes)
                ->orderByRaw("FIND_IN_SET(external_id,'$idsImploded')")->lists('id');
                return $obProductList->intersect($arResultIDList);
            }
            
        });

        $obProductList->addDynamicMethod('getByPort', function ($id = null) use ($obProductList) {
            
            if($id !=null ){
                $arResultIDList = (array) Product::where("port_id", $id)->lists('id');
                return $obProductList->intersect($arResultIDList);
            }
            
        });

        $obProductList->addDynamicMethod('getByBrandIds', function ($arrayCodes = null) use ($obProductList) {
            $idsImploded = implode(',',$arrayCodes);
            
            if($arrayCodes !=null ){
                $arResultIDList = (array) Product::whereIn("brand_id", $arrayCodes)
                ->orderByRaw("FIND_IN_SET(brand_id,'$idsImploded')")->lists('id');
                return $obProductList->intersect($arResultIDList);
            }
            
        });

        $obProductList->addDynamicMethod('hasOffers', function () use ($obProductList) {
            
            
                $arResultIDList = (array) Product::has("offer")->lists('id');
                return $obProductList->intersect($arResultIDList);
            
            
        });
    }
}