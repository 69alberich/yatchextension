<?php namespace Qchsoft\yatchextension\Classes\Event\Product;



use Lovata\Shopaholic\Models\Product as ProductModel;
use Lovata\Shopaholic\Controllers\Products as ProductController;

/**
 * Class ExtendProductFieldsHandler
 * @package Lovata\BaseCode\Classes\Event\Product
 */
class ExtendProductFieldsHandler {

    public function subscribe(){

        ProductController::extend(function($controller) {
          

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/yatchextension/config/product_upselling_relation.yaml';

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );
        });

        

        ProductController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof ProductModel) {
               
                return;
            }
          
            $form->addFields([
                'hull_year' => [
                    'label'   => 'Hull year',
                    'type'    => 'number',
                    'span' => 'auto'
                    
                ],
                'length' => [
                    'label' => 'Length',
                    'type' => 'number',
                    'span' => 'auto'
                ],
                'capacity' =>[
                    'label' => 'Capacity',
                    'type' => 'number',
                    'span' => 'auto'
                ],
                'port' =>[
                    'label' => 'Port',
                    'type' => 'relation',
                    'span' => 'auto',
                    'nameFrom' => 'name',
                ],
                'model' => [
                    'label'   => 'Model',
                    'type'    => 'text',
                    'span' => 'auto'
                ],
            ]);
            
            $form->addTabFields([
                /*'upsellings' => [
                    'tab' => 'Upsellings/Extras',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_upselling_relation.htm',
                    'context' => ['update', 'preview']
                ],*/
                'destinations' => [
                    'tab' => 'Destinations',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_destination_relation.htm',
                    'context' => ['update', 'preview']
                ],
                'attributes' => [
                    'tab' => 'Attributes',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_attribute_relation.htm',
                    'context' => ['update', 'preview']
                ],
                'booking_type' => [
                    'tab' => 'Booking Types',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_bookingtype_relation.htm',
                    'context' => ['update', 'preview']
                ],

                'calendars' => [
                    'tab' => 'Calendars',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_calendars_relation.htm',
                    'context' => ['update', 'preview']
                ],

                'booking_rules' => [
                    'tab' => 'Booking Rules',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_product_bookingrule_relation.htm',
                    'context' => ['update', 'preview']
                ],
                
            ]);
           
        });

       
    }
}

/*

upsellings:
    label: Upsellings
    view:
        list: $/hesperiaplugins/hoteles/models/upselling/columns.yaml
        toolbarButtons: 'add|remove'
        showCheckboxes: true
    pivot:
        form: $/hesperiaplugins/hoteles/models/upselling/paquete_pivot_fields.yaml
        recordsPerPage: 10
    manage:
        scope: disponibleEnPaquete
*/