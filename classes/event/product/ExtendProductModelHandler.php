<?php namespace Qchsoft\yatchextension\Classes\Event\Product;

use Lovata\Shopaholic\Models\Product as ProductModel;
use Db;
use Carbon\Carbon;

class ExtendProductModelHandler{

    public function subscribe(){

        ProductModel::extend(function($model) {
            $model->belongsTo['port'] = ['\Qchsoft\Yatchextension\Models\Port',
             'key' => 'port_id'];
            
             $model->belongsToMany['upsellings'] = ['\Qchsoft\Yatchextension\Models\Upselling',
             'table' => 'qchsoft_yatchextension_product_upselling',
             'pivot' => 'is_free'
            ];

            $model->hasMany['booking_rules'] = [
              '\Qchsoft\Yatchextension\Models\BookingRule',
            ];

            $model->belongsToMany['destinations'] = [
            '\Qchsoft\Yatchextension\Models\Destination',
             'table' => 'qchsoft_yatchextension_product_destination',
             'pivot' => 'conditions'
            ];

            $model->belongsToMany['attributes'] = [
                '\Qchsoft\Yatchextension\Models\Attribute',
                 'table' => 'qchsoft_yatchextension_product_attribute'
            ];

            $model->belongsToMany['booking_type'] = [
            '\Qchsoft\Yatchextension\Models\BookingType',
                'table' => 'qchsoft_yatchextension_product_bookingtype',
                'pivot' => 'is_multidate' 
            ];
            

            $model->morphMany["calendars"] = ['\Qchsoft\Yatchextension\Models\Calendar',
             'name' => 'calendarable'];

             $model->fillable[] = 'hull_year';
             $model->fillable[] = 'length';
             $model->fillable[] = 'capacity';
             $model->fillable[] = 'model';
             $model->fillable[] = 'destination';
             $model->fillable[] = 'port';

             $model->addCachedField(['hull_year', 'length', 'capacity', 'model', 'destination', 'port',
             'getMinPrice']);
            
             $model->addDynamicMethod('getMinPrice', function($arParams = null) use($model) {
               if(isset($arParams["dates"])){
                $arrDates = explode(" - ", $arParams["dates"]);
                $begin = new Carbon ($arrDates[0]);
                $end = new Carbon ($arrDates[1]);

                $price = Db::table("qchsoft_yatchextension_dateprice as dateprice")
                ->join("qchsoft_yatchextension_calendardate as calendardate",
                "dateprice.date_id" ,"=", "calendardate.id")
                ->join('qchsoft_yatchextension_calendar as calendar', function ($join) use ($model) {
                    $join->on('calendardate.calendar_id', '=', 'calendar.id')
                        ->where('calendar.calendarable_type',"Lovata\Shopaholic\Models\Product")
                        ->where('calendar.calendarable_id', $model->id);
                })  
                ->where("calendardate.date", ">=", $begin)
                ->orWhere("calendardate.date", "<=", $end)->select("calendar.calendarable_id")
                ->min('price');
                
                return $price;
               }else{
                   trace_log("no tengo fechas");
                   return null;
               }
               

            });

            $model->addDynamicMethod('scopeBrands', function($query, $brandsIds = null) {
                return $query->whereIn('brand_id', $brandsIds);  
            });
        });

        
        
    }

}
