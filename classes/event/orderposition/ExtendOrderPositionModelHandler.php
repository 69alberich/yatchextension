<?php namespace Qchsoft\yatchextension\Classes\Event\OrderPosition;

use Lovata\OrdersShopaholic\Models\OrderPosition as OrderPositionModel;
use QchSoft\YatchExtension\Models\Charge;

class ExtendOrderPositionModelHandler {

    public function subscribe(){

        OrderPositionModel::extend(function($model) {

            if (!$model instanceof OrderPositionModel) {
               
                return;
            }
            
            $model->belongsTo["charge"] = [
                Charge::class,
                'key' => 'item_id',
            ];

                        
           
        
        });



    }
}