<?php namespace Qchsoft\YatchExtension\Classes\Event\Category;

use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;

class ExtendCategoryCollectionHandler {

    public function subscribe(){
        CategoryCollection::extend(function ($obCategoryList) {
            $this->addCustomMethod($obCategoryList);
        });
    }

    protected function addCustomMethod($obCategoryList)
    {
        
        $obCategoryList->addDynamicMethod('getByCode', function ($arrayCodes = null) use ($obCategoryList) {

            if($arrayCodes !=null ){
                $arResultIDList = (array) Category::whereIn("code", $arrayCodes)->lists('id');
                return $obCategoryList->intersect($arResultIDList);
            }
            
        });
    }
}