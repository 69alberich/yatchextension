<?php namespace QchSoft\YatchExtension\Classes\Event\Order;

use Lovata\Toolbox\Classes\Event\ModelHandler;
use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\OrdersShopaholic\Models\OrderPosition as OrderPositionModel;
use Lovata\Shopaholic\Models\Settings;
use Qchsoft\YatchExtension\Models\Charge;
use Qchsoft\YatchExtension\Models\Payment;
use Lovata\Buddies\Models\User;
use Carbon\Carbon;

class ExtendOrderModelHandler extends ModelHandler{

    public function subscribe($obEvent){
        
        OrderModel::extend(function($model) {

            if (!$model instanceof OrderModel) {
               
                return;
            }
            //pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            /*if (!$model->exists) {
              
                return;
            }
            //'detalles' => ['HesperiaPlugins\Hoteles\Models\DetalleReservacion', 'key' => 'reservacion_id'],

            /*'order_offer'           => [
                OrderPosition::class,
                'condition' => 'item_type = \Lovata\Shopaholic\Models\Offer',
            ],*/

            
            
            $model->hasMany["order_charge"] = [OrderPositionModel::class,
                'condition' => 'item_type = Qchsoft\Charges\Models\Charge',
            ];

            $model->hasMany["payments"]= [Payment::class];

            /*$model->belongsTo["manager"] = [
                User::class,
                'key'=> 'manager_id'
            ];*/
                        
            /*$model->addDynamicMethod('isAvailableToPay', function() use ($model) {
                
                $quantity = $model->property["vigency_value"];
                $format = $model->property["vigency_format"];
                
                $now = Carbon::now();
                if($format == 'hour'){
                    $length = $model->created_at->diffInHours($now);
                }elseif($format == 'day'){
                    $length = $model->created_at->diffInDays($now);
                }elseif ($format == 'week') {
                    $length = $model->created_at->diffInWeeks($now);
                }

                if ($length >  $quantity) {
                    return false;
                }else{
                    return true;
                }
            });*/

            $model->addDynamicMethod('isAvailableToPay', function() use ($model) {
                //trace_log("entre aqui");
                //$obItem->setAttribute('isAvailableToPay', $obModel->created_at);
                $settings = Settings::instance();
                $quantity = $settings->orders_number;
                $format = $settings->orders_code;
                $created_date = $model->created_at->setTimezone("America/Caracas");
                $now = Carbon::now()->setTimezone("America/Caracas");
              
                if($format == 'd'){
                    $length = $created_date->diffInDays($now);      
                }elseif($format == 'h'){
                    $length = $created_date->diffInHours($now);
                }elseif ($format == 'w') {
                    $length = $created_date->diffInWeeks($now);
                }
                
                if ($length >=  $quantity) {
                    
                    return false;
                    
                }else{
                    return true;
                }
                
            });
            
            $model->addDynamicMethod('getExpirationDate', function() use ($model){
                $settings = Settings::instance();
                $quantity = $settings->orders_number;
                $format = $settings->orders_code;

                $created_date = $model->created_at->setTimezone("America/Caracas");
                $now = Carbon::now()->setTimezone("America/Caracas");

                $expiration_date = $model->created_at;

                if($format == 'd'){

                    $length = $created_date->diffInDays($now);
                    $expiration_date->addDays($quantity);
                    

                }elseif($format == 'h'){

                    $length = $created_date->diffInHours($now);
                    $expiration_date->addHours($quantity);
                    

                }elseif ($format == 'w') {

                    $length = $created_date->diffInWeeks($now);
                    $expiration_date->addWeeks($quantity);

                }

                return $expiration_date;
            });
        });
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass()
    {
        return Order::class;
    }

    /**
     * Get item class name
     * @return string
     */
    protected function getItemClass()
    {
        return OrderItem::class;
    }
}