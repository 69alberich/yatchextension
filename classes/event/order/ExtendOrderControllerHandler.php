<?php namespace Qchsoft\YatchExtension\Classes\Event\Order;

use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\OrdersShopaholic\Controllers\Orders as OrdersController;
use Lovata\OrdersShopaholic\Classes\Event\Order\OrderModelHandler;
//use  Qchsoft\YatchExtension\Models\Payment;
//use QchSoft\YatchExtension\Models\Settings as ShopPlusSettings;
use QchSoft\YatchExtension\Classes\Processor\CustomOrderProcessor;
use Event;
use Mail;
use Flash;
use Redirect;
use BackendAuth;

class ExtendOrderControllerHandler extends OrderModelHandler{

    public function subscribe($obEvent){

        OrdersController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            
            if (!$model instanceof OrderModel) {
               
                return;
            }
           /* pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            if (!$model->exists) {
              
                return;
            }
            */
            /*$user = BackendAuth::getUser();
            if (!$user->hasAccess("change-order-status")) {
               // trace_log("no tengo we");
                $form->removeField("status");
            }*/
            $form->removeTab("lovata.ordersshopaholic::lang.tab.tasks");
            $form->removeTab("lovata.ordersshopaholic::lang.tab.offers_info");
            $user = BackendAuth::getUser();

            /*if(!$user->is_superuser){
                $form->removeTab("lovata.ordersshopaholic::lang.tab.info");
                if(!$user->hasAccess("allow-tab-offers")){
                    $form->removeTab("lovata.ordersshopaholic::lang.tab.offers_info");
                }
               
                $form->removeTab("lovata.ordersshopaholic::lang.field.user");
                $form->removeTab("lovata.ordersshopaholic::lang.tab.shipping_address");
            }*/

           /* if($user->hasAccess("allow-tab-offers")){
                //trace_log()
                $form->addTabFields("lovata.ordersshopaholic::lang.tab.offers_info");
            }*/
            $form->removeField("shipping_type");
            $form->removeField("full_shipping_price");

            $form->addTabFields([
                'charge' => [
                    'tab' => 'Charges',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_order_charge_relation.htm',
                    'context' => 'update'
                ]
                
            ]);

            $form->addFields(["actions_block" => [
                'span' => 'left',
                'type' => 'partial',
                'path' => '~/plugins/qchsoft/yatchextension/partials/_actions_block.htm',
                'context' => ['update', 'preview']
                ]
            ]);
            
            $form->addTabFields([
                'resume' => [
                    'tab' => 'Resume',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_order_resume.htm',
                    'context' => ['update', 'preview']
                ],
                
                'payments' => [
                    'tab' => 'Payments',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/yatchextension/partials/_order_payment_relation.htm',
                    'context' => ['update', 'preview']
                ],
                
            ]);
            
        });

        OrdersController::extend(function($controller) {
            
            $this->addDynamicMethods($controller);
            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/yatchextension/config/order_charge_relation.yaml';
            $myPaymentConfigPath = '$/qchsoft/yatchextension/config/order_payment_relation.yaml';
            
            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myPaymentConfigPath
            );

            $user = BackendAuth::getUser();
            if($user){
                if(!$user->hasAccess("change-order-status")){
                    $myPreviewConfigPath = '$/qchsoft/yatchextension/config/order_controller_config_list.yaml';
                    $controller->listConfig =  $myPreviewConfigPath;
                }
            }
        });

        OrdersController::extendListColumns(function($list, $model){
            if (!$model instanceof OrderModel) {
                return;
            }
            $list->removeColumn("created_at");
            $list->removeColumn("updated_at");
  
            $list->addColumns([
                'created_at' => [
                    'label' => 'lovata.toolbox::lang.field.created_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y | h:i:s A"
                ],
                'updated_at' => [
                    'label' => 'lovata.toolbox::lang.field.updated_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y | h:i:s A"
                ],
            ]);
        });

    }
    
    private function addDynamicMethods($controller){
        

        $controller->addDynamicMethod('onSendPreOrder', function($id) use ($controller) {
            //$user = BackendAuth::getUser();
        
            $obOrder = OrderModel::find($id);
                        
            $arOrder = [
                'order'        => $obOrder,
                'order_number' => $obOrder->order_number,
                'site_url'     => config('app.url'),
            ];
            //trace_log();
            Mail::send("lovata.ordersshopaholic::mail.create_order_user",
                $arOrder, function($message) use ($arOrder, $obOrder) {
                    $message->to($obOrder->user->email);
                
            });
            
            Flash::success("Proceso completado");
        });

        $controller->addDynamicMethod('onCancelOrder', function($id) use ($controller) {
            $data = post();
            $obOrder = OrderModel::find($id);

            if($obOrder->status_id == 4){
                Flash::warning("no se puede cancelar la orden porque  ya ha sido cancelada");
            }else{
                $obOrder->status_id = 4;
                $obOrder->save();

                if(isset($data["Order"]["restore_inventory"])){
                    CustomOrderProcessor::instance()->updateCalendar($obOrder, "restore");
                }
                $controller->initForm($obOrder, 'preview');
                Flash::success("Proceso completado");
            } 
        });

        $controller->addDynamicMethod('onChangeStatus', function($id) use ($controller) {
            $data = post();
            $obOrder = OrderModel::find($id);

            if($obOrder->status_id == $data["new_status_id"]){
                Flash::warning("El status ya ha sido cambiado");
            }else{
                $obOrder->status_id = $data["new_status_id"];
                $obOrder->save();
            }
            $controller->initForm($obOrder, 'preview');
            Flash::success("Proceso completado");
            
        });

        
        
        $controller->addDynamicMethod('onApproveOrder', function($id) use ($controller) {
            //PEDIDO ENTREGADO AL CLIENTE
            $obOrder = OrderModel::find($id);
            $data = post();
            $this->obElement = $obOrder;
            
            if($obOrder->status_id == 2 || $obOrder->status_id == 5){
                
                if(isset($data["Order"]["reduce_inventory_approvement"])){
                    CustomOrderProcessor::instance()->updateCalendar($obOrder, "reduce");
                }

                $obOrder->status_id = 3;
                $obOrder->save();
                //$this->sendUserEmailAfterCreating();
                $controller->initForm($obOrder, 'preview');
                Flash::success("Proceso completado");
            }elseif($obOrder->status_id == 7){
        
                $this->createUpdatePaymentForOrder();
                $obOrder->payment_method_id = 4;
                $obOrder->save();

                if (in_array(2, $config)) {
                    foreach ($obOrder->order_position as $order_position) {
                         $order_position->offer->quantity -= $order_position->quantity;
                         $order_position->offer->save();
                     }
                     $obOrder->save();
                 }

                $controller->initForm($obOrder, 'preview');
                Flash::success("Proceso completado");

            }else{
                Flash::error("No se puede aprobar esta orden, refresca esta pantalla e intenta nuevamente");
            }
        });
        /*
        $controller->addDynamicMethod('onChangeForShipping', function($id) use ($controller, $user) {
            $obOrder = OrderModel::find($id);

            $config = ShopPlusSettings::get("discount_inventory_when");

            if ($user->hasAccess("set-order-for-shipping") || $user->hasAccess("change-order-status") ) {
                $payment = $obOrder->payments->first();
                $payment->payment_status_id = 1;
                $payment->save();

                if (is_array($config) && in_array(1, $config)) {
                    foreach ($obOrder->order_position as $order_position) {
                        $order_position->offer->quantity -= $order_position->quantity;
                        $order_position->offer->save();
                    }
                }
                

                $obOrder->status_id = 5;
                $obOrder->save();
                $this->obElement = $obOrder;
                $this->sendUserEmailAfterCreating();

                Flash::success("Proceso completado");
                $controller->initForm($obOrder, 'preview');
             }else{
                Flash::error("aplicar esta opción, refresca esta pantalla e intenta nuevamente");
            }
        });
        */
    }
    

    /*
    private function createUpdatePaymentForOrder(){
        
        $payment = $this->obElement->payments->first();

        if($payment){
            $payment->payment_status_id = 1;
            $payment->save();
        }else{

            $nospace = str_replace(" ", "", $this->obElement->total_price);
            $mount = str_replace(",", ".", $nospace);

            $payment = new Payment();

            $payment->order_id = $this->obElement->id;
            $payment->mount = $mount;
            $payment->reference = "Pago en tienda";
            $payment->payment_method_id = 4;
            $payment->currency =  $this->obElement->currency_id;
            $payment->payment_status_id = 1;

            $this->obElement->payments()->add($payment);
        }
        $this->obElement->payment_method_id = 4;
        $this->obElement->status_id = 3;
        $this->obElement->save();

    }
    */
}