<?php namespace QchSoft\YatchExtension\Classes\Processor;

use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Models\Product;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use QchSoft\YatchExtension\Models\Charge;
use QchSoft\YatchExtension\Models\Calendar;
use QchSoft\YatchExtension\Models\CalendarDate;
use October\Rain\Support\Traits\Singleton;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\Buddies\Models\User;
use BackendAuth;
use Db;

class CustomOrderProcessor{

    use Singleton;

    protected $obOrder;
    protected $obUser;
    protected $obManager;
    protected $arOrderData;
    protected $arCharges;
    protected $messages = "";
    
    public function createOrder($arCharges, $arOrderData, $user){
       
        $this->fillManager(); /*LISTO*/ 

        $this->fillOrderData($arOrderData); /*LISTO*/

        $this->fillUser($arOrderData);
        $this->fillCharges($arCharges);
     
        Db::connection()->enableQueryLog();

        Db::beginTransaction();

        $this->createOrderObject();

        $queries = Db::getQueryLog();

        //trace_log($this->obUser->name);

        Db::commit();

        return $this->obOrder;
    }

    public function createOrderObject(){
        //trace_log($this->arOrderData);
        //trace_log($this->arOrderData);
        $this->obOrder = Order::create($this->arOrderData);

        foreach ($this->arCharges as $key => $charge) {
            $arrayDates = explode(" - ", $charge["dates"]);
            if (count($arrayDates) ==2) {
              $firstDate = new \DateTime($arrayDates[0]);
              $enddate = new \DateTime($arrayDates[1]);
            }else{
              $firstDate = new \DateTime($arrayDates[0]);
              $enddate = new \DateTime($arrayDates[0]);
            }

            $charge["first_date"]=$firstDate;
            $charge["last_date"]=$enddate;
            $charge["product_type"]= "Lovata\Shopaholic\Models\Product";
            $obCharge = Charge::create($charge);
           
           
            try {
                $obOrderPosition = OrderPosition::create([
                    
                    'item_id'=> $obCharge->id,
                    'order_id'=> $this->obOrder->id,
                    'item_type'=> Charge::class,
                    'price'=> $charge["price"],
                    'quantity'=> 1

                ]);
                $this->obOrder->order_position()->add($obOrderPosition);
            } catch (\October\Rain\Database\ModelException $obException) {
                trace_log($obException->getErrors());
                //$this->processValidationError($obException);
                return false;
            }

        }
        $this->obOrder->user()->add($this->obUser);
        $this->obOrder->save();
    }

    public function fillUser(){
        if(isset($this->arOrderData["property"]['email'])){
            $sEmail = $this->arOrderData["property"]['email'];
            $this->obUser = UserHelper::instance()->findUserByEmail($sEmail);
        }else{
            $this->obUser = UserHelper::instance()->getUser();
            $this->arOrderData["property"]['email'] = $this->obUser->email;
        }
        
        
        if (empty($this->obUser)) {
            $userData["email"] = $this->arOrderData["property"]['email'];
            $userData["name"] =  $this->arOrderData["property"]['name'];
            $userData["last_name"] =  $this->arOrderData["property"]['last_name'];
            $userData["password"] =  "RTY2021";
            $userData["password_confirmation"] =  "RTY2021";
            $this->obUser = User::create($userData);
        }
    }
   
    public function fillManager(){
        $user = BackendAuth::getUser();
        if ($user) {
            $this->obManager = $user;
            $this->arOrderData["manager_id"] = $user->id;
        }
        
    }

    public function fillCharges($arCharges){
        
        $this->arCharges = $arCharges;
    }

    public function fillOrderData($arOrderData){
        
        $arOrderData["phone"] = $arOrderData["country_code"].$arOrderData["phone"];
        $arOrderData["document"] = $arOrderData["document_code"].$arOrderData["document"];
        $sCurrencyCode = CurrencyHelper::instance()->getActive();
        $this->arOrderData["status_id"] = 1;
        $this->arOrderData["currency_id"] = $sCurrencyCode->id;
        $this->arOrderData["property"] = $arOrderData;
       
    }

    public static function updateCalendar($obOrder, $type){
        $calendar = null;
        foreach ($obOrder->order_position as $order_position) {

            $charge = $order_position->charge;
            $firstDate = new \DateTime($charge->first_date);
            $lastDate = new \DateTime($charge->last_date);

            $dates = CalendarDate::whereBetween("date",[
                $firstDate->format("Y-m-d"),
                $lastDate->format("Y-m-d"),
            ])->where("calendar_id", $charge->calendar_id)->get();
            
            foreach ($dates as $obDate) {
               // trace_log($obDate);
               if ($type =="reduce") {
                $obDate->quantity -= $order_position->quantity;
                $obDate->save();
               }elseif($type=="restore"){
                $obDate->quantity += $order_position->quantity;
                $obDate->save();
               }
                
            }
        }
        //$obOrder->save();
    }
}