<?php namespace QchSoft\YatchExtension\Classes\Processor;

use DB;
use Carbon\Carbon;
class SearchProductProcessor{

    public static function search($query, $limit = null){

        $products = Db::table("lovata_shopaholic_products as products")
        ->leftJoin('lovata_shopaholic_categories as categories', 
                  'products.category_id', '=', 'categories.id')
        ->leftJoin('lovata_shopaholic_brands as brands',
                  'products.brand_id', "=", "brands.id" )
        ->select("products.name as product_name", "categories.name as category_name,", 
        "brands.name as brand_name", "products.id")
        ->whereRaw("lower(products.name) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.code) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.search_synonym) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.slug) LIKE  \"%$query%\" ")
        ->orWhereRaw("lower(categories.name) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(brands.name) LIKE  \"%$query%\" ")
        ->when($limit, function($query, $limit){
            return $query->take($limit);
        })
        ->orderBy('products.name', 'asc')
        ->get(); 
        
        $arrayIds = array();

        $arrayIds = $products->map(function($item, $key){
            return $item->id;
        });

        return $arrayIds->all();

        //trace_log($products);
    }

    public static function searchByDates($params, $limit = null){
        $arrDates = explode(" - ", $params["dates"]);

        $begin = new \DateTime($arrDates[0]);
        $end = new \DateTime($arrDates[1]);

        $products = Db::table("lovata_shopaholic_products as products")
        ->join('qchsoft_yatchextension_calendar as calendar', function ($join) {
            $join->on('products.id', '=', 'calendar.calendarable_id')
                ->where('calendar.calendarable_type',"Lovata\Shopaholic\Models\Product");
        })
        ->join('qchsoft_yatchextension_calendardate as calendardate',
         'calendar.id', '=', 'calendardate.calendar_id')

         ->join('qchsoft_yatchextension_product_destination as productDestination',
         'products.id', '=', 'productDestination.product_id')

         ->join('qchsoft_yatchextension_destination as destination',
         'destination.id', '=', 'productDestination.destination_id')
         
         ->join('lovata_shopaholic_categories as categories',
         'categories.id', '=', 'products.category_id')


         ->where("calendardate.quantity", ">", 0)
         
         ->when($limit, function($query, $limit){
            return $query;
        })
        ->when($params, function($query, $params){
            if ($params["destination"] != "all") {
                return $query->where("destination.slug", $params["destination"]);
            }
        })
        ->when($params, function($query, $params){
            if ($params["category"] != "all") {
                return $query->where("categories.code", $params["category"]);
            }
        })
        ->whereBetween("calendardate.date", [$begin->format("Y-m-d"), $end->format("Y-m-d")])
        ->select("calendar.calendarable_id")
        ->get();
        $arrayIds = array();
        $arrayIds = $products->map(function($item, $key){
            //trace_log($item->id);
            return $item->calendarable_id;
            //array_push($arrayIds, ;
        });
        return $arrayIds->all();
         //
    }

    public static function getBoatPrice($post, $subtrac = false){

        $arrDates = explode(" - ", $post["dates"]);
        
        $result = Db::table("qchsoft_yatchextension_calendardate as calendardate")
        ->join("qchsoft_yatchextension_calendar as calendar", "calendardate.calendar_id",
         "=", "calendar.id")
         ->join("qchsoft_yatchextension_dateprice as dateprice", "calendardate.id",
        "=", "dateprice.date_id")
        ->join("qchsoft_yatchextension_destination as destination", "destination.id",
        "=", "dateprice.destination_id")
        ->join("qchsoft_yatchextension_bookingtype as bookingType", "bookingType.id",
        "=", "dateprice.booking_type_id")
        ->join("lovata_shopaholic_products as product", "product.id",
        "=", "calendar.calendarable_id")
        ->join("qchsoft_yatchextension_product_bookingtype as productBookingType", function ($join) use($post) {
            $join->on("productBookingType.product_id", "=", "product.id")
            ->where("productBookingType.booking_type_id", "=", $post["bookingType"]);
        })

        ->where("calendar.id", $post["calendar_id"])
        ->where("dateprice.destination_id", $post["destination"])
        ->where("dateprice.booking_type_id", $post["bookingType"])
        ->where("calendardate.quantity", ">", 0)
        ->when($arrDates, function($query, $arrDates) use ($subtrac){
            if (count($arrDates)==2) {
                $begin = new \DateTime($arrDates[0]);
                $end = new \DateTime($arrDates[1]);
                if ($subtrac) {
                    $end->sub(new \DateInterval('P1D'));
                }
                return $query->whereBetween("calendardate.date", [$begin->format("Y-m-d"), $end->format("Y-m-d")]);

            }elseif(count($arrDates)==1){
                $begin = new \DateTime($arrDates[0]);
                return $query->where("calendardate.date", $begin->format("Y-m-d"));
            }
            
           
        })
        ->select("dateprice.price", "calendardate.date", 
        "destination.name as destination", "bookingType.name as bookingType",
        "product.name as productName", "product.id as productId", 
        "destination.id as destination_id", "bookingType.id as booking_type_id",
        "calendar.id as calendar_id", "productBookingType.is_multidate"
        )
        ->get();

        return $result;
    }

    public static function getBookingProductMode($post){
        $result = Db::table("qchsoft_yatchextension_product_bookingtype as productBookingType")
        ->where("productBookingType.product_id", $post["id"])
        ->where("productBookingType.booking_type_id", $post["bookingType"])
        ->select("is_multidate")
        ->first();
        
        return $result;
    }

    public static function getMinDaysForBooking($post){
        $result = Db::table("qchsoft_yatchextension_booking_rules as bookingRules")
        ->where("bookingRules.product_id", $post["id"])
        ->where("bookingRules.booking_type_id", $post["bookingType"])
        ->where("bookingRules.destination_id", $post["destination"])
        ->select("min_days")
        ->first();

        return $result;
    }
}


