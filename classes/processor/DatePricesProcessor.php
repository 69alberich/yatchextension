<?php namespace QchSoft\YatchExtension\Classes\Processor;

use QchSoft\YatchExtension\Models\CalendarDate;
use QchSoft\YatchExtension\Models\DatePrice;
use Db;
class DatePricesProcessor{
    private $beginDate;
    private $endDate;
    private $calendarId;
    private $quantity;

    public function generateDates($form){
        $this->init($form);

        $interval = new \DateInterval('P1D');

        $daterange = new \DatePeriod($this->beginDate, $interval, $this->endDate);
        //trace_log($form);
        foreach($daterange as $date){
            if(in_array($date->format("N"), $form["days_of_week"])){

                $date = CalendarDate::firstOrNew([
                    "calendar_id" => $this->calendarId,
                    "date" => $date->format("Y-m-d")
                ]);
                $date->quantity = $this->quantity;
                $date->save();
    
                $datePrice = DatePrice::FirstOrNew([
                    "date_id" => $date->id,
                    "booking_type_id" => $form["booking_type"],
                    "currency_id" => 1,
                    "destination_id" => $form["destination"]
                ]);
    
                $datePrice->price = $form["price"];
                $datePrice->save();
            }/*else{
                trace_log("me salto:".$date->format("d-m-Y"));
            }*/
           
        }   
    }

    public function setAvailability($form){
        $this->init($form);
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($this->beginDate, $interval, $this->endDate);

        DB::table('qchsoft_yatchextension_calendardate')
          ->where("calendar_id", "=", $this->calendarId)
          ->whereBetween('date', [
            $this->beginDate->format("Y-m-d"),
            $this->endDate->format("Y-m-d")])
          ->update(['quantity' =>$this->quantity]);
    }

    public function init($form){
        
        $endDate = new \DateTime($form["date_to"]);
        $endDate->add(new \DateInterval('P1D'));

        $this->beginDate = new \DateTime($form["date_from"]);
        $this->endDate = $endDate;
        $this->calendarId = $form["calendar_id"];
        $this->quantity = $form["quantity"];
    }

}