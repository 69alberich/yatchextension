<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionProductAttribute extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_product_attribute', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('product_id');
            $table->integer('attribute_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_product_attribute');
    }
}
