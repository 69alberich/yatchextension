<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionProductDestination extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_product_destination', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('destination_id');
            $table->text('conditions');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_product_destination');
    }
}
