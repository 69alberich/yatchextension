<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionDateprice2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_dateprice', function($table)
        {
            $table->integer('destination_id');
           
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_dateprice', function($table)
        {
            $table->dropColumn('destination_id');
           
        });
    }
}
