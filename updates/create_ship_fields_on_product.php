<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class create_ship_fields_on_product extends Migration
{
    const TABLE_NAME = 'lovata_shopaholic_products';
    
    public function up()
    {
        // Schema::create('qchsoft_yatchextension_table', function($table)
        // {
        // });
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'my_field')) {
            return;
        }
        
         Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            $obTable->integer('port_id');
            $obTable->string('model');
            $obTable->decimal('length', 10, 2);
            $obTable->integer('hull_year');
            $obTable->integer('capacity');
            
        });
    }

    public function down()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'my_field')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('port_id');
            $obTable->dropColumn('model');
            $obTable->dropColumn('length');
            $obTable->dropColumn('hull_year');
            $obTable->dropColumn('capacity');
        });
    }
}