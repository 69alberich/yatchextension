<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionCalendar extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_calendar', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('calendarable_id');
            $table->string('calendarable_type');
            $table->string('name');
            $table->text('description');
            $table->string('code');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_calendar');
    }
}
