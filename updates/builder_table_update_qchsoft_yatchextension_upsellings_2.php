<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionUpsellings2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_upsellings', function($table)
        {
            $table->string('slug');
           
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_upsellings', function($table)
        {
            $table->dropColumn('slug');
           
        });
    }
}
