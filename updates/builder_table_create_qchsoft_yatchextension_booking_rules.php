<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionBookingRules extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_booking_rules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('booking_type_id');
            $table->integer('destination_id');
            $table->integer('min_days')->default(1);
            $table->index(['booking_type_id', 'destination_id'], 'rules_index');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_booking_rules');
    }
}
