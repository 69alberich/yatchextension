<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionDateprice extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_dateprice', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->decimal('price', 10, 2);
            $table->integer('date_id');
            $table->integer('booking_type_id');
            $table->integer('currency_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_dateprice');
    }
}
