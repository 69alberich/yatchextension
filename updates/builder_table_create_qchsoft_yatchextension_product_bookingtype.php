<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionProductBookingtype extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_product_bookingtype', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('booking_type_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_product_bookingtype');
    }
}
