<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionCharges extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_charges', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('destination_id')->nullable();
            $table->integer('booking_type_id')->nullable();
            $table->integer('calendar_id')->nullable();
            $table->date('first_date')->nullable();
            $table->date('last_date')->nullable();
            $table->integer('product_id');
            $table->smallInteger('product_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_charges');
    }
}
