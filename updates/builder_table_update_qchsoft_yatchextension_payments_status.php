<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionPaymentsStatus extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_payments_status', function($table)
        {
            $table->text('description')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_payments_status', function($table)
        {
            $table->smallInteger('description')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
