<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionBookingRules extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_booking_rules', function($table)
        {
            $table->integer('product_id');
            $table->dropIndex('rules_index');
            $table->index(['booking_type_id', 'destination_id', 'product_id'], 'rules_index');
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_booking_rules', function($table)
        {
            $table->dropColumn('product_id');
        });
    }
}
