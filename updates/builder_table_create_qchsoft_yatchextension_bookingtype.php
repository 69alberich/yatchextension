<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionBookingtype extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_bookingtype', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_bookingtype');
    }
}
