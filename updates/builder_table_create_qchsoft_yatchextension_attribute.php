<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionAttribute extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_attribute', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->smallInteger('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_attribute');
    }
}
