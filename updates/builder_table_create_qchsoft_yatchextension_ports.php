<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionPorts extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_ports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('code', 150);
            $table->string('name', 150);
            $table->string('slug', 200);
            $table->text('description');
            $table->text('preview_text');
            $table->text('address');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_ports');
    }
}
