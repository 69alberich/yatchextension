<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionCharges extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_charges', function($table)
        {
            $table->decimal('price', 10, 2);
            $table->integer('destination_id')->default(0)->change();
            $table->integer('booking_type_id')->default(0)->change();
            $table->integer('calendar_id')->default(0)->change();
            $table->date('first_date')->default(null)->change();
            $table->date('last_date')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_charges', function($table)
        {
            $table->dropColumn('price');
            $table->integer('destination_id')->default(NULL)->change();
            $table->integer('booking_type_id')->default(NULL)->change();
            $table->integer('calendar_id')->default(NULL)->change();
            $table->date('first_date')->default('NULL')->change();
            $table->date('last_date')->default('NULL')->change();
        });
    }
}
