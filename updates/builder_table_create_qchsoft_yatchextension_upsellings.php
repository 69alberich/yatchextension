<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionUpsellings extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_upsellings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->string('code');
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_upsellings');
    }
}
