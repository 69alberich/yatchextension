<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionCharges2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_charges', function($table)
        {
            $table->integer('destination_id')->nullable(false)->default(0)->change();
            $table->integer('booking_type_id')->nullable(false)->default(0)->change();
            $table->integer('calendar_id')->nullable(false)->default(0)->change();
            $table->date('first_date')->nullable(false)->default(null)->change();
            $table->date('last_date')->nullable(false)->default(null)->change();
            $table->string('product_type', 150)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_charges', function($table)
        {
            $table->integer('destination_id')->nullable()->default(NULL)->change();
            $table->integer('booking_type_id')->nullable()->default(NULL)->change();
            $table->integer('calendar_id')->nullable()->default(NULL)->change();
            $table->date('first_date')->nullable()->default('NULL')->change();
            $table->date('last_date')->nullable()->default('NULL')->change();
            $table->smallInteger('product_type')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
