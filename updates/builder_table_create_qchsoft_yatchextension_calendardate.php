<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionCalendardate extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_calendardate', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('calendar_id');
            $table->date('date');
            $table->integer('quantity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_calendardate');
    }
}
