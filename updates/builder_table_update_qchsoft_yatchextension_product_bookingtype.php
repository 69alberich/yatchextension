<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftYatchextensionProductBookingtype extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_yatchextension_product_bookingtype', function($table)
        {
            $table->integer('is_multidate')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_yatchextension_product_bookingtype', function($table)
        {
            $table->dropColumn('is_multidate');
        });
    }
}
