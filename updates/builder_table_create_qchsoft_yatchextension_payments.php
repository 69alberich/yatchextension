<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionPayments extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_payments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('order_id');
            $table->decimal('mount', 15, 2);
            $table->string('reference');
            $table->integer('payment_method_id');
            $table->integer('currency_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('payment_status_id');
            $table->integer('usable_id');
            $table->smallInteger('usable_type');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_payments');
    }
}
