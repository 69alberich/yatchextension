<?php namespace Qchsoft\YatchExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftYatchextensionProductUpselling extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_yatchextension_product_upselling', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('upselling_id');
            $table->integer('is_free')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_yatchextension_product_upselling');
    }
}
